<?php
/**
 * Created by PhpStorm.
 * User: mallmann
 * Date: 01/05/18
 * Time: 14:55
 */

namespace controllers;


use models\Item;

class ExamplesController
{
    public function paginator() : void
    {
        $item = new Item();

        $limit = 10;

        $total = $item->getTotal();

        $data['paginas'] = ceil($total/$limit);
        $data['paginaAtual'] = 1;
        if (!empty($_GET['p'])){
            $data['paginaAtual'] = intval($_GET['p']);
        }
        $offset = ($data['paginaAtual'] * $limit) - $limit;
        $data['lista'] = $item->getList($offset, $limit);

        $this->loadTemplate('examples/paginator', $data);
    }

}