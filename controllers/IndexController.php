<?php
/**
 * Created by PhpStorm.
 * User: mallmann
 * Date: 08/04/18
 * Time: 16:50
 */

namespace controllers;

use core\Controller;
use models\Item;

class IndexController extends Controller
{
    public function index()
    {
//        $item = new Item();
//
//        $limit = 10;
//
//        $total = $item->getTotal();
//
//        $data['paginas'] = ceil($total/$limit);
//        $data['paginaAtual'] = 1;
//        if (!empty($_GET['p'])){
//            $data['paginaAtual'] = intval($_GET['p']);
//        }
//        $offset = ($data['paginaAtual'] * $limit) - $limit;
//        $data['lista'] = $item->getList($offset, $limit);
//
//        $this->loadTemplate('index/index', $data);
        $this->loadTemplate('index/index');
    }

}